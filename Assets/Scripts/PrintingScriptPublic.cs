﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintingScriptPublic : MonoBehaviour
{

    public float Speed = 10;
    public float Range = 20;
    public int Health = 1000;
    public string PrintMessage = "Hello!";
    public bool IsShown = true;
    
    void Start()
    {
        Debug.Log(Speed);
        Debug.Log(Range);
        Debug.Log(Health);
        Debug.Log(PrintMessage);
        Debug.Log(IsShown);
        

    }

    
   
}
